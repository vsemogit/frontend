const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '/',
        component: () => import('pages/Index.vue')
      },
      {
        path: '/torgash',
        component: () => import('pages/Torgash.vue')
      },
      {
        path: '/kassir',
        component: () => import('pages/Kassir.vue')
      },
      {
        path: '/seller',
        component: () => import('pages/Seller.vue')
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
